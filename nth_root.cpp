/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <chrono>
#include <iostream>

/** Calcul of the n-th root of a number > 1;
 * param: number The number we need the root of
 * param: n the number of the root
 */

double getNthRoot(double number, int n);

/**
 * Calcul of the power of a number (number ^ pow)
 * Only valid for positive power
 */ 
double power(double number, int pow);


int main()
{
    int n = 3;
    double number = 12344784;
    
    auto start = std::chrono::high_resolution_clock::now();
    double root = getNthRoot(number, n);
    auto end = std::chrono::high_resolution_clock::now();
    
    std::cout.precision(7);
    std::cout << "The " << n << "-th root of " << number << " is " << std::fixed << root << "\n";
    std::cout << "Calcul time : " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() << " ns\n";

    return 0;
}


double getNthRoot(double number, int n)
{
    //We calculate the n-th root using Newton's method (https://en.wikipedia.org/wiki/Nth_root#Computing_principal_roots)
    static double TARGET_ACCURACY = 1e-5;
    static int MAX_ITERATION = 100;
    static double INITIAL_ESTIMATE = 5;
    
    double x = INITIAL_ESTIMATE;
    double delta = 100;
    int iteration = 0;
    
    while(delta > TARGET_ACCURACY && ++iteration < MAX_ITERATION)
    {
        double new_x = x * (n - 1)/n + number / (n * power(x, n-1));
        delta = new_x - x;
        delta *= delta > 0 ? 1.0 : -1.0;
        x = new_x;
    }
    return x;
}

double power(double number, int pow)
{
    if(pow < 0) throw new std::invalid_argument("Function invalid for negative power");
    double res = 1;
    for(int i = 0; i < pow; i++)
    {
        res *= number;
    }
    return res;
}

