/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <map>
#include <set>
#include <random>

std::string randomString();

int main()
{
    std::cout << "start\n";
    
    std::map<int, std::set<std::string>> hash_to_string;
    int string_gener = 0;
    
    std::hash<std::string> hasher;

    while(true)
    {
        std::string s = randomString();
        string_gener++;
        int hash_val = hasher(s);
        hash_to_string[hash_val].insert(s);
        if(hash_to_string[hash_val].size() == 3)
        {
            for(auto s : hash_to_string[hash_val])
            {
                std::cout << s << "\n";
            }
            break;
        }
        if(hash_to_string.size() % 1000 == 0)
        {
            std::cout << "string_gener : " << string_gener << "\n";
            std::cout << "hash table size : " << hash_to_string.size() << std::endl;
        }
    }

    return 0;
}


std::string randomString()
{
    static std::string pool("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
    static int max_rd = pool.size() - 1;
    static std::random_device rd;
    int SIZE_STRING = 5;
    std::string str;
    
    for(int i = 0; i < SIZE_STRING; i++)
    {
        str += pool[(rd()%(max_rd))];
    }
    return str;
}
